const HomePage = require('../pom/pages/HomePage');
const global_data = require('../../fixtures/global_data.json');
const profile_data = require('../../fixtures/profile.json');
const product_data = require('../../fixtures/products.json');

before(() => {
    HomePage.Open(global_data.baseUrl);
    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    });
});

describe('as a user i should be able add new item cart and proceed to cart page', function () {
    it('validate the cart added item name, size and price', function () {
        HomePage
            .SelectAllBrands()
            .SearchBrand(product_data[0].brand)
            .ShopNowWithBrand()
            .SignUpForProducts(profile_data.gender, profile_data.email)
            .SelectProduct(product_data[0].name)
            .AddProductToCart(product_data[0].size)
            .NavigateToCart()
            .VerifyProductNameInCartPage(product_data[0].name)
            .VerifyProductPriceInCartPage(product_data[0].price)
            .VerifyProductSizeInCartPage(product_data[0].size)
    });
});
const BasePage = require('../../../utils/BasePage');

const lblAllBrands = "button ~ div > a[href='/all-brands']";

const HomePage = function () {

    /**
     * Navigate to base page
     * @param {string} baseURL 
     * @returns HomePage
     */
    this.Open = function (baseURL) {
        BasePage.NavigateTo(baseURL);
        return require('../pages/HomePage');
    }

    /**
     * Select All Brands Option
     * @returns AllBrandsPage
     */
    this.SelectAllBrands = function () {
        BasePage.Click(lblAllBrands);
        return require('../pages/AllBrandsPage');
    };
}
module.exports = new HomePage();
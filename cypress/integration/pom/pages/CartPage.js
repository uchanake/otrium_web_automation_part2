const BasePage = require('../../../utils/BasePage');

const lblSize = "[data-testid='cart-item-size']"
const lblProductName = ".css-1vmlcaf"
const lblPrice = "[data-testid='cart-item-price'] span"

const CartPage = function () {

    /**
     * Verify produc name in cart page
     * @param {string} name 
     * @returns CartPage
     */
    this.VerifyProductNameInCartPage = function (name) {
        BasePage.WaitForElementPresent(lblProductName);
        BasePage.CheckElementValue(lblProductName, name);
        return this;
    }

    /**
     * Verify produc size in cart page
     * @param {string} size 
     * @returns CartPage
     */
    this.VerifyProductSizeInCartPage = function (size) {
        BasePage.WaitForElementPresent(lblSize);
        BasePage.CheckElementValue(lblSize, size);
        return this;
    }

    /**
     * Verify produc price in cart page
     * @param {string} price 
     * @returns CartPage
     */
    this.VerifyProductPriceInCartPage = function (price) {
        BasePage.WaitForElementPresent(lblPrice);
        BasePage.CheckElementValue(lblPrice, price);
        return this;
    }

}
module.exports = new CartPage();
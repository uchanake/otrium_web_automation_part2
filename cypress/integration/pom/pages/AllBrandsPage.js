/// <reference types="cypress"/>

const BasePage = require('../../../utils/BasePage');

const btnSearch = "div.header.toggle.search";
const txtSearch = ".col-active > .menu-wrap > .search-brands";
const cbxSearchResult = ".col-active > .menu-wrap > .menu > .brand-list > :nth-child(4)";
const btnSave = ".show-menu .search-brands ~ a";
const btnShopNow = ".brand-action > .action-featured-black";
const btnDeleteAll = ".reset-desktop";

const AllBrandPage = function () {

    /**
     * Action: to search brand
     * @param {string} brandName 
     */
    const actionSearchBrand = function (brandName) {
        BasePage.Click(btnSearch);
        BasePage.Type(txtSearch, brandName);
        BasePage.Click(cbxSearchResult);
        BasePage.Click(btnSave);
    }

    /**
     * Search for given brand
     * @param {string} brandName 
     * @returns AllBrandsPage
     */
    this.SearchBrand = function (brandName) {
        actionSearchBrand(brandName);
        return this;
    }

    /**
     * Shop now with specific brands
     * @returns OutletPage
     */
    this.ShopNowWithBrand = function () {
        BasePage.WaitForElementPresent(btnDeleteAll);
        BasePage.Click(btnShopNow);
        return require('../pages/OutletPage');
    }
}
module.exports = new AllBrandPage();
const BasePage = require('../../../utils/BasePage');
const TopNavBar = require('../panels/TopNavBar.js');

const btnAddToCart = "[data-testid=product-order-button]";
const drpSize = "[data-testid='product-order-select-size']";
const optSize = function (size) {
    return ".css-cz9mfx:contains('" + size + "')";
}

const ProductDetailsPage = function () {

    /**
     * Action: to set size sku for procust
     * @param {string} size 
     */
    const actionSetSize = function (size) {
        BasePage.Click(drpSize);
        BasePage.Click(optSize(size));
    }

    /**
     * Add sku product to cart
     * @param {string} size 
     * @returns ProductDetailsPage
     */
    this.AddProductToCart = function (size) {
        actionSetSize(size);
        BasePage.Click(btnAddToCart);
        return this;
    }

    /**
     * Navigate to cart page
     * @returns CartPage
     */
    this.NavigateToCart = function () {
        TopNavBar.NavigateToCart();
        return require('../pages/CartPage.js');
    }
}
module.exports = new ProductDetailsPage();
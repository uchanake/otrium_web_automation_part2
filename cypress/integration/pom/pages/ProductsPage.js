const BasePage = require('../../../utils/BasePage');

const eleResults = "[data-testid=products-sorting] > [data-testid=total-products]";
const btnClosePopUp = ".Modal__content_slide--false > [data-testid=optin-wrapper] > .css-o4m37w > .css-1r9qpxv > [data-testid=close-cta] > svg";
const btnProduct = function (productName) {
    return "[data-testid='product-card'] a div:contains('" + productName + "')";
}

const ProductPage = function () {

    /**
     * Action: to remove the sales pop-up
     */
    const actionRemovePopUp = function () {
        BasePage.WaitForElementPresent(btnClosePopUp);
        BasePage.Click(btnClosePopUp);
    }

    /**
     * Select specific product
     * @param {string} productName 
     * @returns ProductDetailsPage
     */
    this.SelectProduct = function (productName) {
        actionRemovePopUp();
        BasePage.CheckElementPresent(eleResults);
        BasePage.Click(btnProduct(productName));
        return require('../pages/ProductDetailsPage');
    }
}
module.exports = new ProductPage();
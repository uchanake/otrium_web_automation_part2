const BasePage = require('../../../utils/BasePage');

const btnWomen = ".css-4cffwv > :nth-child(2)";
const btnMale = ".css-4cffwv > :nth-child(2)";
const btnKids = ".css-4cffwv > :nth-child(2)";
const txtEmail = "input[placeholder='Email address']";
const btnCreateACcount = ".css-1tnalob"

const OutletPage = function () {

    /**
     * Action: to create account
     * @param {string} gender 
     * @param {string} email 
     */
    const actionCreateAccount = function (gender, email) {
        switch (gender) {
            case 'men':
                BasePage.Click(btnMale);
                break;
            case 'women':
                BasePage.Click(btnWomen);
                break;
            default:
                BasePage.Click(btnKids);
                break;
        }
        BasePage.Type(txtEmail, email);
        BasePage.Click(btnCreateACcount);
    }

    /**
     * Sign up for porduct brands
     * @param {string} gender 
     * @param {string} email 
     * @returns ProductsPage
     */
    this.SignUpForProducts = function (gender, email) {
        actionCreateAccount(gender, email);
        return require('../pages/ProductsPage');
    }
}
module.exports = new OutletPage();
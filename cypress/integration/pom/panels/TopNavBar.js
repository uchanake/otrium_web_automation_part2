const BasePage = require('../../../utils/BasePage');

const btnCart = "[data-testid=main-header-cart-a] > svg";

const TopNavBar = function () {

    /**
     * Navigate to cart page
     */
    this.NavigateToCart = function () {
        BasePage.Click(btnCart);
    }
}
module.exports = new TopNavBar();
class BasePage {

    /**
     * Navigate to given URL
     * @param {string} baseUrl 
     */
    NavigateTo = function (baseUrl) {
        cy.visit(baseUrl);
    }

    /**
     * Click on element
     * @param {string} uiElementPath 
     */
    Click = function (uiElementPath) {
        const element = cy.get(uiElementPath);
        element.scrollIntoView();
        element.should('be.visible');
        element.click({force: true});
    }

    /**
     * Type on element
     * @param {string} uiElementPath 
     * @param {string} userInput 
     */
    Type = function (uiElementPath, userInput) {
        const element = cy.get(uiElementPath);
        element.should('be.visible');
        element.should('be.enabled');
        element.type(userInput);
    }

    /**
     * Check Element Present
     * @param {string} uiElementPath 
     */
    CheckElementPresent = function (uiElementPath) {
        const element = cy.get(uiElementPath);
        element.should('be.visible');
    }

    /**
     * Wait for Element Visible (max 10secs.)
     * @param {string} uiElementPath 
     */
    WaitForElementPresent = function (uiElementPath) {
        const element = cy.get(uiElementPath, { timeout: 10000 }).should('be.visible');
        element.should('be.visible');
    }

    /**
     * Check Element Present
     * @param {string} uiElementPath 
     */
    CheckElementNotPresent = function (uiElementPath) {
        const element = cy.get(uiElementPath);
        element.should('not.be.visible');
    }

    /**
     * Check Element Text
     * @param {string} uiElement 
     * @param {string} text 
     */
    CheckElementValue = function (uiElement, text) {
        const element = cy.get(uiElement);
        element.should('have.text', text);
    }
}
module.exports = new BasePage();